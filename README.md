# Example library

Example kicad database library

# Usage
The database uses [alembic](https://alembic.sqlalchemy.org/en/latest/) for database migration.

## Create a new migration
```bash
export "DATABASE_URL=postgresql://<username>:<password>@<host>/<dbname>"
alembic revision -m "<some message>"
```

## Upgrade database to latest revision
```bash
export "DATABASE_URL=postgresql://<username>:<password>@<host>/<dbname>"
alembic upgrade head
```

## Downgrade last revision
```bash
export "DATABASE_URL=postgresql://<username>:<password>@<host>/<dbname>"
alembic downgrade -1
```

# License
This library is licensed under GPLv3.

See [LICENSE](./LICENSE)
