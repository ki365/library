# CI/CD utility script to create a dummy Ki365 database
# TODO: Replace with testing against an actual ki365 instance

import json
import os
from dotenv import load_dotenv
from sqlalchemy import create_engine, inspect
from sqlalchemy.engine.interfaces import ReflectedColumn

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def check_column_exists(column_name: str, columns: list[ReflectedColumn], column_type: str | None = None) -> bool:
    column = next((column for column in columns if column['name'] == column_name), None)

    if not column:
        print(f"{bcolors.FAIL}\tMissing column {column_name}{bcolors.ENDC}")
        return False
    
    res = True
    if column_type:
        res &= column['type'] == column_type
    
    return res

if __name__ == "__main__":
    load_dotenv()


    # Get kicad dbl
    with open('ki365.kicad_dbl') as kicad_dbl_file:
        dbl = json.load(kicad_dbl_file)
    
    # Get database schemas
    url = os.environ["DATABASE_URL"]
    engine = create_engine(url, echo=False)
    inspector = inspect(engine)
    tables = inspector.get_table_names()
     
    error = False
    for lib in dbl['libraries']:
        lib_name = lib['name']
        lib_table = lib['table']
        print(f"{bcolors.HEADER}Checking library '{lib_name}'{bcolors.ENDC}")

        if lib_table not in tables:
            error = True
            print(f"{bcolors.FAIL}\tMissing table {lib_table}{bcolors.ENDC}")
            continue
        
        columns = inspector.get_columns(lib_table)
        error |= not check_column_exists(lib['key'], columns)
        error |= not check_column_exists(lib['symbols'], columns)
        error |= not check_column_exists(lib['footprints'], columns)
        
        for field in lib['fields']:
            field_name = field['name']
            error |= not check_column_exists(field['column'], columns)
        
        if 'properties' in lib:
            properties: dict[str, str] = lib['properties']
            for prop in properties.values():
                error |= not check_column_exists(prop, columns)
    
    if error:
        print("Failed!")
        exit(1)
    else:
        print("Success")
