"""rename-resistor-capacitor

Revision ID: fcd20931f63d
Revises: 4cfc965a28b9
Create Date: 2023-06-17 15:11:43.507882

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fcd20931f63d'
down_revision = '4cfc965a28b9'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.rename_table("resistors", "lib_resistors")
    op.rename_table("capacitors", "lib_capacitors")
    

def downgrade() -> None:
    op.rename_table("lib_resistors", "resistors")
    op.rename_table("lib_capacitors", "capacitors")