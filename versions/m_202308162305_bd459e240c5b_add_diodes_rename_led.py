"""add_diodes_rename_led

Revision ID: bd459e240c5b
Revises: d4c946268d54
Create Date: 2023-08-16 23:05:13.258016

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bd459e240c5b'
down_revision = 'd4c946268d54'
branch_labels = None
depends_on = None


def upgrade() -> None:
    
    # My library table
    op.rename_table("lib_led_diodes", "lib_diodes_led")

    op.create_table(
        "lib_diodes_rectifier",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("type", sa.TEXT),
        sa.Column("current", sa.TEXT),
        sa.Column("forward_voltage", sa.TEXT),
        sa.Column("reverse_voltage", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )

    op.create_table(
        "lib_diodes_zener",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("zener_voltage", sa.TEXT),
        sa.Column("tolerance", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.rename_table("lib_diodes_led", "lib_led_diodes")
    op.drop_table("lib_diodes_rectifier")
    op.drop_table("lib_diodes_zener")


