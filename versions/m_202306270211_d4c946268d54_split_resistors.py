"""split_resistors

Revision ID: d4c946268d54
Revises: a744672a4be2
Create Date: 2023-06-27 02:11:19.116282

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd4c946268d54'
down_revision = 'a744672a4be2'
branch_labels = None
depends_on = None


def upgrade() -> None:

    # Rename the old resistor table to "resistors_misc" and add a type column
    op.rename_table("lib_resistors", "lib_resistors_misc")
    op.add_column("lib_resistors_misc", sa.Column("type", sa.TEXT))
    
    op.create_table(
        "lib_resistors_thinfilm",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields
        sa.Column("value", sa.TEXT),
        sa.Column("power", sa.TEXT),
        sa.Column("tolerance", sa.TEXT),
        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )

    op.create_table(
        "lib_resistors_thickfilm",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields
        sa.Column("value", sa.TEXT),
        sa.Column("power", sa.TEXT),
        sa.Column("tolerance", sa.TEXT),
        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.rename_table("lib_resistors_misc", "lib_resistors")
    op.drop_column("lib_resistors", "type")

    op.drop_table("lib_resistors_thickfilm")
    op.drop_table("lib_resistors_thinfilm")


