"""add_operational_amplifiers

Revision ID: 1e9433c7579f
Revises: bd459e240c5b
Create Date: 2023-08-27 03:56:38.412176

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1e9433c7579f'
down_revision = 'bd459e240c5b'
branch_labels = None
depends_on = None


def upgrade() -> None:
    
    # My library table
    op.create_table(
        "lib_operational_amplifiers",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("channels", sa.TEXT),
        sa.Column("supply_voltage", sa.TEXT),
        sa.Column("gain_bandwidth_product", sa.TEXT),
        sa.Column("input_noise_density", sa.TEXT, nullable=True),
        sa.Column("input_offset_voltage", sa.TEXT, nullable=True),
        sa.Column("common_mode_rejection_ratio", sa.TEXT, nullable=True),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.drop_table("lib_operational_amplifiers")
