"""add-memory-regulators

Revision ID: 1ddfc966d734
Revises: 74e4e80c83ac
Create Date: 2023-06-17 13:26:13.887015

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1ddfc966d734'
down_revision = '74e4e80c83ac'
branch_labels = None
depends_on = None


def upgrade() -> None:
    
    # My library table
    op.create_table(
        "lib_linear_regulators",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("input_voltage", sa.TEXT),
        sa.Column("output_voltage", sa.TEXT),
        sa.Column("current", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    op.create_table(
        "lib_switched_regulators",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("input_voltage", sa.TEXT),
        sa.Column("output_voltage", sa.TEXT),
        sa.Column("current", sa.TEXT),
        sa.Column("frequency", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.drop_table("lib_linear_regulators")
    op.drop_table("lib_switched_regulators")

