# CI/CD utility script to create a dummy Ki365 database
# TODO: Replace with testing against an actual ki365 instance

import os
from dotenv import load_dotenv
from sqlalchemy import create_engine

from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column



class Base(DeclarativeBase):
    pass

class Parts(Base):
    __tablename__ = "parts"

    id: Mapped[int] = mapped_column(primary_key=True)


if __name__ == "__main__":
    load_dotenv()

    url = os.environ["DATABASE_URL"]
    engine = create_engine(url, echo=True)
    
    # Create all required tables
    Base.metadata.create_all(engine)